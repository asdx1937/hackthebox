# HackTheBox🥺

## Getting started

Go and get the OSCP!!

And become a Penetration tester!!

[This is where I want to work.](https://devco.re/recruit/#RT_Specialist)

## OSCP Machines

**Scope**: [OSCP like machines](https://docs.google.com/spreadsheets/u/1/d/1dwSMIAPIam0PuRBkCiDI88pU3yzrqqHkDtBngUHNCw8/htmlview#)

## Methodology
- ### [🔎Reconnaissance](#reconnaissance-1)
- ### [📜Enumeration](#enumeration-1)
- ### [🔪Exploit](#exploit-1)
- ### [🚀Privilege Escalation](#privilege-escalation-1)

## [🔎Reconnaissance](#methodology)
### **Goal**: to scan all ports on <targetip>

### **Process**:
- ##### [recon_masscan_tcp](#recon_masscan_tcp-1)
- ##### [recon_masscan_udp](#recon_masscan_udp-1)

## [📜Enumeration](#methodology)
### **Goal**: to find service and version details

### **Process**:
- ##### [enumerate_nmap_tcp](#enumerate_nmap_tcp-1)
- ##### [enumerate_nmap_udp](#enumerate_nmap_udp-1)

## [🔪Exploit](#methodology)
### **Goal**: gain interactive access on <targetip>

### **Process**:
- ##### 
- ##### 

## [🚀Privilege Escalation](#methodology)
### **Goal**: gain elevated privileges on <targetip>

### **Process**:
- ##### 
- ##### 

## 🔥TTPs
- ### [recon_masscan_tcp](#methodology)
run masscan full tcp scans

`sudo masscan -e tun0 -p0-65535 -oG masscan_all_tcp <targetip>`

---
- ### [recon_masscan_udp](#methodology)
run masscan full udp scans

`sudo masscan -e tun0 -pU:0-65535 -oG masscan_all_udp <targetip>`

---
- ### [enumerate_nmap_tcp](#methodology)
run nmap full tcp scans

`nmap -sC -sV -oN tcp_scan <targetip>`

---
- ### [enumerate_nmap_udp](#methodology)
run nmap full udp scans

`nmap -sU -sC -sV -oN udp_scan <targetip>`

## Shout out to
#### [7h3rAm](https://github.com/7h3rAm/writeups)
